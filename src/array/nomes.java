package array;

import java.util.Scanner;
import java.util.Arrays;

public class nomes {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String[] nomes = new String[10];
		
		System.out.printf("Insira %d nomes: \n",nomes.length);
		for(int i = 0; i < nomes.length; i++) {
			System.out.printf("%d:",i+1);
			nomes[i] = input.nextLine();
		}
		
		Arrays.sort(nomes);
		System.out.println("\nOrdenados: ");
		for(String name : nomes) {
			System.out.println(name);
		}
		
		input.close();
		
	}
}
