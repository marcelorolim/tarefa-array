package array;

public class array300{
	public static void main(String[] args) {
		int[] arr; 

		// 1. 300 posicoes com 45
		arr = new int[300];
		for(int i=0; i < 300; i++){
			arr[i] = 45;
			System.out.println(arr[i]); // 2.1
		}

		// 2. Imprimir de 3 formas diferentes (uma já foi)
		// 2.2
		System.out.print("\nMatrix (15x15)\n[");
		int i;
		for (i = 0; i < 300; i++) {
			if(i % 15 == 0){
				System.out.println();
			}
			System.out.print(arr[i] + " ");
		}
		System.out.println("\n]\n");

		// 2.3
		while(--i >= 0){
			System.out.printf("%03d-%d\n",i,arr[i]);
		}

	}
}