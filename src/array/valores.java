package array;

import java.util.Scanner;
import java.util.Arrays;

public class valores {
	public static void main(String[] args) {
		int[] conjunto = new int[10];
		Scanner input = new Scanner(System.in);
		int valor = 0;
		
		for(int i = 0; i < conjunto.length; i++) {
			System.out.printf("Valor #%02d: ", i+1);
			conjunto[i] = input.nextInt();
		}
		
		System.out.print("Buscar por: ");
		valor = input.nextInt();
		
		Arrays.sort(conjunto);
		
		int found = Arrays.binarySearch(conjunto, valor);
		if(found >= 0) {
			System.out.printf("Encontrado\nindex=%d\n",found);
		}else {
			System.out.println("Valor nao encontrado.");
		}		
		
		input.close();
	}
}
