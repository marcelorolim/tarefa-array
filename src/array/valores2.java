package array;

import java.util.Scanner;
import java.util.Arrays;

public class valores2 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int[] valores = new int[10];
		
		System.out.printf("Insira %d valores: \n",valores.length);
		for(int i = 0; i < valores.length; i++) {
			System.out.printf("%d:",i+1);
			valores[i] = input.nextInt();
		}
		
		Arrays.sort(valores);
		System.out.println("\nOrdenados: ");
		for(int name : valores) {
			System.out.println(name);
		}
		input.close();
	}
}
