package usuario;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		User[] users = new User[10];
		String nome = "";
		
		for(int i = 0; i < users.length; i++) {
			System.out.printf("Nome do usuario %02d: ",i+1);
			nome = input.nextLine();
			users[i] = new User(nome);
			System.out.printf("ID gerada: %d\n\n", users[i].getId());
		}
		
		System.out.println("\nALL USERS");
		for (User user : users) {
			System.out.println(user);
		}
		
		input.close();
	}
}
