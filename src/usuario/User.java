package usuario;

public class User {
	private static int _nextId = 1;
	private int id;
	private String username;
	
	public User(String username) {
		this.setUsername(username);
		this.id = _getNextId();
	}
	
	private int _getNextId() {
		try {
			return _nextId;
		}finally {
			_incrementId();
		}
	}
	
	private void _incrementId() {
		_nextId++;
	}
	
	public int getId() {
		return this.id;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String toString() {
		return String.format("Id:%d|Username:%s",this.id,this.username);
	}
}
